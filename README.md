
### 預け入れ

```mermaid
flowchart
    classDef greenBg fill:#219308;

    callOpenDoor[ <解錠-預け入れ用API> をcall]:::greenBg
    callOrderAPI[< Box予約API><br>をCall]:::greenBg
    callGetOpenCode[<コード取得API>をCall]:::greenBg
    doOpen[開扉]
    doClose[施錠]
    doCancel[当Boxの利用を<br>キャンセルする]
    doFinish[当Boxの預入処理を<br>完了する]
    setObj[荷物を入れる]
    hasObj{当Boxに物<br>があるか}
    hasOrderId{当Boxが<br>予約済か}

    callOpenDoor--> doOpen
    doOpen-->callOrderAPI
    callOrderAPI-->doClose
    doOpen-->setObj
    setObj-->doClose
    setObj<-->callOrderAPI
    doOpen-->doClose
    doClose-->hasObj
    hasObj-->|No| doCancel
    hasObj-->|Yes| hasOrderId
    hasOrderId-->|No| doOpen
    hasOrderId-->|Yes| doFinish
    doFinish-->callGetOpenCode

```


当フォロー図の作成にmermaidを利用しています。[使い方](https://mermaid-js.github.io/mermaid/#/flowchart?id=interaction)
